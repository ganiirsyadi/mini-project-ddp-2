package com.gais.bmiCalc.utils;

import java.time.Instant;
import java.util.Random;

public class Utils {
    public static long generateId() {
        Random rand = new Random();
        return Instant.now().getEpochSecond() + rand.nextInt(1000);
    }

    public static long offsetFromPageAndSize(long page, long size) {
        long offset = (page - 1) * size;
        if (offset < 0) {
            return 0;
        }
        return offset;
    }
}
