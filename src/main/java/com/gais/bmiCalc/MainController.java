package com.gais.bmiCalc;

import com.gais.bmiCalc.model.BmiModel;
import com.gais.bmiCalc.repository.BmiRepository;
import com.gais.bmiCalc.utils.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class MainController {
    BmiRepository r = new BmiRepository("db.json");

    @GetMapping("/")
    public String index(Model model, @CookieValue(value = "userId", defaultValue = "") String userId, HttpServletResponse response) {

        // Set new cookie for new users
        if(userId.equals("")) {
            userId = Long.toString(Utils.generateId());
            Cookie cookie = new Cookie("userId", Long.toString(Utils.generateId()));
            cookie.setMaxAge(30*24*60*60);
            response.addCookie(cookie);
        }

        List<BmiModel> results = r.findBmiByUserID(Long.parseLong(userId));

        model.addAttribute("bmiModel", new BmiModel());
        model.addAttribute("results", results);

        return "index";
    }

    @GetMapping("/result")
    public String result(Model model) {
        model.addAttribute("bmiModel", new BmiModel());
        return "index";    
    }
    
    @PostMapping("/result")
    public String submitForm(@ModelAttribute BmiModel bmiModel,  @CookieValue(value = "userId", defaultValue = "") String userId) {
        if (bmiModel.getBmi() == 0) {
            return "invalidInput";
        }
        if(!userId.equals("")) {
            bmiModel.userID = Long.parseLong(userId);
            r.createBmi(bmiModel);
        }
        return "result";
    }

    @GetMapping("/clear")
    public String clearHistory(Model model, HttpServletResponse response) throws Exception {
        model.addAttribute("bmiModel", new BmiModel());

        Cookie cookie = new Cookie("userId", null);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        response.sendRedirect("/");

        return null;
    }
}