package com.gais.bmiCalc.repository;

import com.gais.bmiCalc.BmiCalcApplication;
import com.gais.bmiCalc.MainController;
import com.gais.bmiCalc.model.BmiModel;
import com.gais.bmiCalc.utils.Utils;
import com.google.gson.Gson;

import java.io.*;
import java.nio.Buffer;
import java.util.*;
import java.util.stream.Collectors;

public class BmiRepository {
    private Hashtable<Long, BmiModel> storage = new Hashtable<Long, BmiModel>();
    private String dbFilename;

    public BmiRepository(String dbFilename) {
        this.dbFilename = dbFilename;
    }

    public void createBmi(BmiModel req) {
        this.storage.put(req.getBmiId(), req);
        this.writeToFile();
    }

    public List<BmiModel> findBmiByUserID(long userID) {
        this.readFile();
        List<BmiModel> res = new ArrayList<BmiModel>();
        for(BmiModel b:this.storage.values()) {
            if(b.userID == userID) {
                res.add(b);
            }
        }

        return res;
    }

    void writeToFile() {
        try {
            Gson gson = new Gson();
            String jsonResult = gson.toJson(new ArrayList<BmiModel>(this.storage.values()));
            try {
                FileWriter fw = new FileWriter(this.dbFilename, false);
                fw.write(jsonResult);
                fw.flush();
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    void readFile() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(this.dbFilename));
            String jsonResult = br.lines().collect(Collectors.joining());
            Gson gson = new Gson();
            BmiModel[] bmiArr = gson.fromJson(jsonResult, BmiModel[].class);
            this.storage.clear();
            for (BmiModel b : bmiArr) {
                this.storage.put(b.getBmiId(), b);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
