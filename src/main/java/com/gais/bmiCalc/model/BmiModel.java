package com.gais.bmiCalc.model;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

public class BmiModel {
    private long bmiId;
    private int height;
    private int weight;
    private String createdAt;
    public long userID;

    public BmiModel() {
        Random rand = new Random();
        this.bmiId = Instant.now().getEpochSecond() + rand.nextInt(1000);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        this.createdAt = formatter.format(date);
    }

    public BmiModel(int height, int weight, String createdAt, long userID) {
        // Generate object's ID
        Random rand = new Random();
        this.bmiId = Instant.now().getEpochSecond() + rand.nextInt(1000);
        this.height = height;
        this.weight = weight;
        this.createdAt = createdAt;
        this.userID = userID;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public long getBmiId() {
        return this.bmiId;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWeight() {
        return this.weight;
    }

    public long getUserID() {
        return this.userID;
    }

    public double getBmi() throws NumberFormatException {
        if (this.getHeight() == 0) {
            return 0;
        } else {
            return this.weight / (Math.pow((double) this.height/100, 2));
        }
    }

    public String getBmiString() {
        return String.format("%.2f", this.getBmi());
    }

    public String getCategory() {
        if (this.getBmi() < 18.5) {
            return "underweight";
        } else if (this.getBmi() < 25) {
            return "normal";
        } else if (this.getBmi() < 30) {
            return "overweight";
        } else {
            return "obese";
        }
    }

    public String toString() {
        return String.format("%d %d %d %s", this.bmiId, this.height, this.weight, this.createdAt);
    }
}
